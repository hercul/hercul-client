<?php

namespace Hercul\Hercul\RequestBuilder;


/**
 * Class AbstractRequestBuilder
 *
 * @package Hercul\Hercul\RequestBuilder
 */
abstract class AbstractRequestBuilder
{
    /** @var RequestManager */
    protected $requestManager;

	/**
	 * @return mixed
	 */
    abstract public function build();

	/**
	 * @param RequestManager $requestManager
	 *
	 * @return $this
	 */
    public function setRequestManager(RequestManager $requestManager)
    {
        $this->requestManager = $requestManager;

        return $this;
    }

	/**
	 * @return mixed
	 */
    public function send()
    {
        return $this->requestManager->sendRequest($this->build());
    }

}
