<?php

namespace Hercul\Hercul\RequestBuilder;

use Hercul\Hercul\Model\CompanyRole;

/**
 * Class UserCompanyRoleRequestBuilder
 *
 * @package Hercul\Hercul\RequestBuilder
 */
class UserCompanyRoleRequestBuilder extends AbstractRequestBuilder
{
	/** @var CompanyRole */
	private $companyRole;

	/** @var string  */
	private $endpoint;

	/**
	 * @param CompanyRole $companyRole
	 *
	 * @return $this
	 */
	public function create(CompanyRole $companyRole)
	{
		$this->endpoint = '/api/external/user/company/role/add';

		$this->companyRole = $companyRole;

		return $this;
	}

	/**
	 * @param CompanyRole $companyRole
	 *
	 * @return $this
	 */
	public function delete(CompanyRole $companyRole)
	{
		$this->endpoint = '/api/external/user/company/role/remove';

		$this->companyRole = $companyRole;

		return $this;
	}

	/**
	 * @return Request|mixed
	 */
	public function build()
	{
		return new Request($this->endpoint, RequestMethodInterface::METHOD_POST, $this->companyRole);
	}
}