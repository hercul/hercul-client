<?php

namespace Hercul\Hercul\RequestBuilder;

use Hercul\Hercul\Model\JobRole;

/**
 * Class UsersRequestBuilder
 *
 * @package Hercul\Hercul\RequestBuilder
 */
class UserJobRoleRequestBuilder extends AbstractRequestBuilder
{
	/** @var JobRole */
	private $jobRole;

	/** @var string  */
	private $endpoint;

	/**
	 * @param JobRole $jobRole
	 *
	 * @return $this
	 */
	public function create(JobRole $jobRole)
	{
		$this->endpoint = '/api/external/user/job/role/add';

		$this->jobRole = $jobRole;

		return $this;
	}

	/**
	 * @param JobRole $jobRole
	 *
	 * @return $this
	 */
	public function delete(JobRole $jobRole)
	{
		$this->endpoint = '/api/external/user/job/role/remove';

		$this->jobRole = $jobRole;

		return $this;
	}

	/**
	 * @return Request|mixed
	 */
	public function build()
	{
		return new Request($this->endpoint, RequestMethodInterface::METHOD_POST, $this->jobRole);
	}
}