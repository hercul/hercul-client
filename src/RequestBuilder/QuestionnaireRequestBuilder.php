<?php

namespace Hercul\Hercul\RequestBuilder;

use Hercul\Hercul\Model\Choice;
use Hercul\Hercul\Model\Questionnaire;

/**
 * Class QuestionnaireRequestBuilder
 *
 * @package Hercul\Hercul\RequestBuilder
 */
class QuestionnaireRequestBuilder extends AbstractRequestBuilder
{

	/** @var  */
	private $endpoint;

	/** @var  */
	private $method;

	/** @var */
	private $payload = null;

	/**
	 * @param Questionnaire $questionnaire
	 *
	 * @return $this
	 */
	public function create(Questionnaire $questionnaire)
	{
		$this->endpoint = '/api/external/questionnaire';
		$this->method = RequestMethodInterface::METHOD_POST;
		$this->payload = $questionnaire;

		return $this;
	}

	/**
	 * @param Questionnaire $questionnaire
	 *
	 * @return $this
	 */
	public function delete(Questionnaire $questionnaire)
	{
		$this->endpoint = '/api/external/questionnaire/' . $questionnaire->getId();
		$this->method = RequestMethodInterface::METHOD_DELETE;

		return $this;
	}

	/**
	 * @param Choice $choice
	 *
	 * @return $this
	 */
	public function createChoice(Choice $choice)
	{
		$this->endpoint = '/api/external/questionnaire/' . $choice->getExternalQuestionnaireId() . '/choice';
		$this->method = RequestMethodInterface::METHOD_POST;
		$this->payload = $choice;

		return $this;
	}

	/**
	 * @param Choice $choice
	 *
	 * @return $this
	 */
	public function removeChoice(Choice $choice)
	{
		$this->endpoint = '/api/external/questionnaire/' . $choice->getExternalQuestionnaireId() . '/choice/' . $choice->getExternalId();
		$this->method = RequestMethodInterface::METHOD_DELETE;

		return $this;
	}

	/**
	 * @param Choice $choice
	 *
	 * @return $this
	 */
	public function updateChoice(Choice $choice)
	{
		$this->endpoint = '/api/external/questionnaire/' . $choice->getExternalQuestionnaireId() . '/choice/' . $choice->getExternalId();
		$this->method = RequestMethodInterface::METHOD_PATCH;
		$this->payload = $choice;

		return $this;
	}

	/**
	 * @return Request|mixed
	 */
	public function build()
	{
		return new Request($this->endpoint, $this->method, $this->payload);
	}
}