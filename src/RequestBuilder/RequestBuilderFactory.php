<?php

namespace Hercul\Hercul\RequestBuilder;


/**
 * Class RequestBuilderFactory
 *
 */
class RequestBuilderFactory
{
    /** @var RequestManager */
    private $requestManager;

	/**
	 * RequestBuilderFactory constructor.
	 *
	 * @param RequestManager $requestManager
	 */
    public function __construct(RequestManager $requestManager)
    {
        $this->requestManager = $requestManager;
    }

	/**
	 * @return CompanyRequestBuilder
	 */
	public function company()
	{
		$requestBuilder = new CompanyRequestBuilder();
		$this->setupBuilder($requestBuilder);

		return $requestBuilder;
	}

	/**
	 * @return JobRequestBuilder
	 */
	public function job()
	{
		$requestBuilder = new JobRequestBuilder();
		$this->setupBuilder($requestBuilder);

		return $requestBuilder;
	}

	/**
	 * @param $shouldDelete
	 *
	 * @return ApplicationRequestBuilder
	 */
	public function application($shouldDelete = false)
	{
		$requestBuilder = new ApplicationRequestBuilder($shouldDelete);
		$this->setupBuilder($requestBuilder);

		return $requestBuilder;
	}

	/**
	 * @return UserRequestBuilder
	 */
	public function user()
	{
		$requestBuilder = new UserRequestBuilder();
		$this->setupBuilder($requestBuilder);

		return $requestBuilder;
	}

	/**
	 * @return UserJobRoleRequestBuilder
	 */
	public function userJobRole()
	{
		$requestBuilder = new UserJobRoleRequestBuilder();
		$this->setupBuilder($requestBuilder);

		return $requestBuilder;
	}

	/**
	 * @return UserCompanyRoleRequestBuilder
	 */
	public function userCompanyRole()
	{
		$requestBuilder = new UserCompanyRoleRequestBuilder();
		$this->setupBuilder($requestBuilder);

		return $requestBuilder;
	}

	/**
	 * @return QuestionnaireRequestBuilder
	 */
	public function questionnaire()
	{
		$requestBuilder = new QuestionnaireRequestBuilder();
		$this->setupBuilder($requestBuilder);

		return $requestBuilder;
	}

	/**
	 * @param AbstractRequestBuilder $requestBuilder
	 */
	private function setupBuilder(AbstractRequestBuilder $requestBuilder)
	{
		$requestBuilder->setRequestManager($this->requestManager);
	}

}
