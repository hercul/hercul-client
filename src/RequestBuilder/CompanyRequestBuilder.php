<?php

namespace Hercul\Hercul\RequestBuilder;

use Hercul\Hercul\Model\Company;

/**
 * Class CompanyRequestBuilder
 *
 * @package Hercul\Hercul\RequestBuilder
 */
class CompanyRequestBuilder extends AbstractRequestBuilder
{
	/** @var string  */
	const ENDPOINT_PATH = '/api/external/company';

	/** @var Company */
	private $company;

	/**
	 * @param Company $company
	 *
	 * @return $this
	 */
	public function create(Company $company)
	{
		$this->company = $company;

		return $this;
	}

	/**
	 * @return Request|mixed
	 */
	public function build()
	{
		return new Request(static::ENDPOINT_PATH, RequestMethodInterface::METHOD_POST, $this->company);
	}
}