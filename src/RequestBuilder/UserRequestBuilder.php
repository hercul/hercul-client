<?php

namespace Hercul\Hercul\RequestBuilder;

use Hercul\Hercul\Model\User;

/**
 * Class UserRequestBuilder
 *
 * @package Hercul\Hercul\RequestBuilder
 */
class UserRequestBuilder extends AbstractRequestBuilder
{
	const ENDPOINT_PATH = '/api/external/user';

	/** @var User */
	private $user;

	/** @var  */
	private $method;

	/**
	 * @param User $user
	 *
	 * @return $this
	 */
	public function create(User $user)
	{
		$this->method = RequestMethodInterface::METHOD_POST;

		$this->user = $user;

		return $this;
	}

	/**
	 * @param User $user
	 *
	 * @return $this
	 */
	public function update(User $user)
	{
		$this->method = RequestMethodInterface::METHOD_PATCH;

		$this->user = $user;

		return $this;
	}

	/**
	 * @return Request|mixed
	 */
	public function build()
	{
		return new Request(static::ENDPOINT_PATH, $this->method, $this->user);
	}
}