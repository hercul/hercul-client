<?php

namespace Hercul\Hercul\RequestBuilder;

use Hercul\Hercul\Model\Job;

/**
 * Class JobRequestBuilder
 *
 * @package Hercul\Hercul\RequestBuilder
 */
class JobRequestBuilder extends AbstractRequestBuilder
{
	const ENDPOINT_PATH = '/api/external/job';

	/** @var Job */
	private $job;

	/**
	 * @param Job $job
	 *
	 * @return $this
	 */
	public function create(Job $job)
	{
		$this->job = $job;

		return $this;
	}

	/**
	 * @return Request|mixed
	 */
	public function build()
	{
		return new Request(static::ENDPOINT_PATH, RequestMethodInterface::METHOD_POST, $this->job);
	}
}