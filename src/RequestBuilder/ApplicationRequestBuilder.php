<?php

namespace Hercul\Hercul\RequestBuilder;

use Hercul\Hercul\Model\Application;
use Hercul\Hercul\Model\Applications;

/**
 * Class ApplicationRequestBuilder
 *
 * @package Hercul\Hercul\RequestBuilder
 */
class ApplicationRequestBuilder extends AbstractRequestBuilder
{
	/** @var */
	private $payload;

	/** @var bool */
	private $shouldDelete = false;

	/** @var string  */
	private $endpoint;

	/**
	 * @param bool $shouldDelete Should the request delete item properties instead of creating new?
	 */
	public function __construct($shouldDelete)
	{
		$this->shouldDelete = $shouldDelete;
	}

	/**
	 * @param Application $application
	 *
	 * @return $this
	 */
	public function create(Application $application)
	{
		$this->endpoint = '/api/external/jobs/' . $application->getJobId() . '/applications';

		$this->payload = $application;

		return $this;
	}

	/**
	 * @param              $adId
	 * @param Applications $applications
	 *
	 * @return $this
	 */
	public function createBatch($adId, Applications $applications)
	{
		$this->endpoint = '/api/external/jobs/' . $adId . '/applications/bulk';

		$this->payload = $applications;

		return $this;
	}

	/**
	 * @param Application $application
	 *
	 * @return $this
	 */
	public function delete(Application $application)
	{
		$this->endpoint = '/api/external/jobs/' . $application->getJobId() . '/applications/' . $application->getId();

		$this->payload = $application;

		return $this;
	}

	/**
	 * @return Request|mixed
	 */
	public function build()
	{
		$method = $this->shouldDelete ? RequestMethodInterface::METHOD_DELETE : RequestMethodInterface::METHOD_POST;
		return new Request($this->endpoint, $method, $this->payload);
	}
}