<?php

namespace Hercul\Hercul\RequestBuilder;

/**
 * Class RequestManager
 *
 * @package Hercul\Hercul\RequestBuilder
 */
class RequestManager
{
	/** @var string */
	private $baseUrl;

	/** @var string */
	private $environment;

	/** @var string */
	protected $accountId;

	/** @var string */
	protected $apiKey;

	/**
	 * RequestManager constructor.
	 *
	 * @param $accountId
	 * @param $apiKey
	 */
	public function __construct($accountId, $apiKey)
	{
		$this->accountId = $accountId;
		$this->apiKey = $apiKey;
	}

	/**
	 * @param Request $request
	 *
	 * @return mixed
	 */
	public function sendRequest(Request $request)
	{
		$url = $this->buildBaseUrl() . $request->getPath();

		$ch = curl_init($url);
		switch ($request->getMethod()) {
			case "GET":
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request->getData()));
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
				break;
			case "POST":
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request->getData()));
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				break;
			case "PATCH":
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request->getData()));
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
				break;
			case "DELETE":
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
				break;
		}

		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Bearer ' . $this->apiKey));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

		$response = curl_exec($ch);
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		$httpError = curl_error($ch);

		curl_close($ch);
		unset($ch);

		return [
			'response' => $response,
			'httpCode' => $httpCode,
			'httpError' => $httpError
		];
	}

	/**
	 * @param $baseUrl
	 */
	public function setBaseUrl($baseUrl)
	{
		$this->baseUrl = $baseUrl;
	}

	/**
	 * @param $environment
	 */
	public function setEnvironment($environment)
	{
		$this->environment = $environment;
	}

	/**
	 * @return string
	 */
	protected function buildBaseUrl()
	{
		return $this->baseUrl . $this->environment;
	}

}
