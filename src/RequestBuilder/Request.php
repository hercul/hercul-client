<?php

namespace Hercul\Hercul\RequestBuilder;

/**
 * Class Request
 *
 * @package Hercul\Hercul\Model
 */
class Request
{
    /** @var string */
    private $path;

    /** @var string */
    private $method;

    /** @var  */
    private $data;

	/**
	 * Request constructor.
	 *
	 * @param $path
	 * @param $method
	 * @param $data
	 */
    public function __construct($path, $method, $data)
    {
        $this->path = $path;
        $this->method = $method;
        $this->data = $data;
    }

	/**
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}

	/**
	 * @return string
	 */
	public function getMethod()
	{
		return $this->method;
	}

	/**
	 * @return mixed
	 */
	public function getData()
	{
		return $this->data;
	}

}
