<?php

namespace Hercul\Hercul;

use Hercul\Hercul\RequestBuilder\RequestManager;
use Hercul\Hercul\RequestBuilder\RequestBuilderFactory;

/**
 * Class Hercul
 *
 * @package Hercul\Hercul
 */
class Hercul
{

	/** @var RequestManager  */
	private $requestManager;

	/**
	 * Hercul constructor.
	 *
	 * @param [] $config
	 */
	public function __construct($config)
	{
		$this->requestManager = new RequestManager($config['accountId'], $config['apiKey']);
		$this->requestManager->setBaseUrl($config['baseUrl']);
		$this->requestManager->setEnvironment($config['environment']);
	}

	/**
	 * @return RequestBuilderFactory
	 */
	public function request()
	{
		return new RequestBuilderFactory($this->getRequestManager());
	}

	/**
	 * @return RequestManager
	 */
	protected function getRequestManager()
	{
		return $this->requestManager;
	}

}