<?php

namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class Questionnaire
 *
 * @package Hercul\Hercul\Model
 */
class Questionnaire implements JsonSerializable
{

	/** @var int  */
	private $id;

	/** @var int */
	private $jobId;

	/** @var  */
	private $jobUuid;

	/** @var  */
	private $title;

	/** @var array  */
	private $questions = [];

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getJobUuid()
	{
		return $this->jobUuid;
	}

	/**
	 * @param mixed $jobUuid
	 */
	public function setJobUuid($jobUuid): void
	{
		$this->jobUuid = $jobUuid;
	}

	/**
	 * @return int
	 */
	public function getJobId(): int
	{
		return $this->jobId;
	}

	/**
	 * @param int $jobId
	 */
	public function setJobId(int $jobId)
	{
		$this->jobId = $jobId;
	}

	/**
	 * @return mixed
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param mixed $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * @return array
	 */
	public function getQuestions(): array
	{
		return $this->questions;
	}

	/**
	 * @param Question $question
	 */
	public function addQuestion(Question $question)
	{
		$this->questions[] = $question;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		return [
			'id' => $this->getId(),
			'jobId' => $this->getJobId(),
			'jobUuid' => $this->getJobUuid(),
			'title' => $this->getTitle(),
			'questions' => $this->getQuestions()
		];
	}

}