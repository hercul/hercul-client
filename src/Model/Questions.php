<?php

namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class Questions
 *
 * @package Hercul\Hercul\Model
 */
class Questions implements JsonSerializable
{

	/** @var []  */
	private $questions = [];

	/**
	 * @return array
	 */
	public function getQuestions(): array
	{
		return $this->questions;
	}

	/**
	 * @param $question
	 */
	public function addQuestion($question)
	{
		$this->questions[] = $question;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		return $this->getQuestions();
	}
}