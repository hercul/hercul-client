<?php

namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class Document
 *
 * @package Hercul\Hercul\Model
 */
class Document implements JsonSerializable
{

	/** @var string */
	private $originalName;

	/** @var string */
	private $url;

	/** @var string */
	private $origin;

	/**
	 * Document constructor.
	 *
	 * @param string $originalName
	 * @param string $url
	 * @param string $origin
	 */
	public function __construct(string $originalName, string $url, string $origin)
	{
		$this->originalName = $originalName;
		$this->url = $url;
		$this->origin = $origin;
	}

	/**
	 * @param string $originalName
	 */
	public function setOriginalName(string $originalName)
	{
		$this->originalName = $originalName;
	}

	/**
	 * @param string $url
	 */
	public function setUrl(string $url)
	{
		$this->url = $url;
	}

	/**
	 * @return string
	 */
	public function getOriginalName(): string
	{
		return $this->originalName;
	}

	/**
	 * @return string
	 */
	public function getUrl(): string
	{
		return $this->url;
	}

	/**
	 * @return string
	 */
	public function getOrigin(): string
	{
		return $this->origin;
	}

	/**
	 * @param string $origin
	 */
	public function setOrigin(string $origin): void
	{
		$this->origin = $origin;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		return [
			'originalName' => $this->getOriginalName(),
			'url' => $this->getUrl(),
			'origin' => $this->getOrigin()
		];
	}
}