<?php

namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class Choice
 *
 * @package Hercul\Hercul\Model
 */
class Choice implements JsonSerializable
{

	/** @var  */
	private $externalQuestionnaireId;

	/** @var  */
	private $externalQuestionId;

	/** @var  */
	private $id;

	/** @var */
	private $externalId;

	/** @var  */
	private $name;

	/** @var  */
	private $value;

	/** @var  */
	private $position;

	/**
	 * @return mixed
	 */
	public function getExternalQuestionnaireId()
	{
		return $this->externalQuestionnaireId;
	}

	/**
	 * @param mixed $externalQuestionnaireId
	 */
	public function setExternalQuestionnaireId($externalQuestionnaireId)
	{
		$this->externalQuestionnaireId = $externalQuestionnaireId;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id): void
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getExternalId()
	{
		return $this->externalId;
	}

	/**
	 * @param mixed $externalId
	 */
	public function setExternalId($externalId)
	{
		$this->externalId = $externalId;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @param mixed $value
	 */
	public function setValue($value)
	{
		$this->value = $value;
	}

	/**
	 * @return mixed
	 */
	public function getPosition()
	{
		return $this->position;
	}

	/**
	 * @param mixed $position
	 */
	public function setPosition($position)
	{
		$this->position = $position;
	}

	/**
	 * @return mixed
	 */
	public function getExternalQuestionId()
	{
		return $this->externalQuestionId;
	}

	/**
	 * @param mixed $externalQuestionId
	 */
	public function setExternalQuestionId($externalQuestionId)
	{
		$this->externalQuestionId = $externalQuestionId;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		$choice = [];

		$id = $this->getId();
		if (!empty($id)) {
			$choice['id'] = $id;
		}

		$externalId = $this->getExternalId();
		if (!empty($externalId)) {
			$choice['externalId'] = $externalId;
		}

		$externalQuestionnaireId = $this->getExternalQuestionnaireId();
		if (!empty($externalQuestionnaireId)) {
			$choice['externalQuestionnaireId'] = $externalQuestionnaireId;
		}

		$choice['questionId'] = $this->getExternalQuestionId();
		$choice[$this->getName()] = $this->getValue();
		$choice['position'] = $this->getPosition();

		return $choice;
	}
}