<?php

namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class JobNotificationStatus
 *
 * @package Hercul\Hercul\Model
 */
class JobNotificationStatus implements JsonSerializable
{

	/** @var  */
	private $adId;

	/** @var bool */
	private $applicationLink;

	/** @var */
	private $notificationType;

	/**
	 * @param mixed $adId
	 */
	public function setAdId($adId): void
	{
		$this->adId = $adId;
	}

	/**
	 * @return mixed
	 */
	public function getAdId()
	{
		return $this->adId;
	}

	/**
	 * @return bool
	 */
	public function getApplicationLink(): bool
	{
		return $this->applicationLink;
	}

	/**
	 * @param bool $applicationLink
	 */
	public function setApplicationLink(bool $applicationLink): void
	{
		$this->applicationLink = $applicationLink;
	}

	/**
	 * @return mixed
	 */
	public function getNotificationType()
	{
		return $this->notificationType;
	}

	/**
	 * @param mixed $notificationType
	 */
	public function setNotificationType($notificationType): void
	{
		$this->notificationType = $notificationType;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		return [
			'externalJobId' => $this->getAdId(),
			'externalApplicationLink' => $this->getApplicationLink(),
			'notificationType' => $this->getNotificationType(),
		];
	}
}