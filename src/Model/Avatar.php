<?php

namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class Avatar
 *
 * @package Hercul\Hercul\Model
 */
class Avatar implements JsonSerializable
{

	/** @var */
	private $url;

	/** @var */
	private $originalName;

	/**
	 * @return mixed
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @param mixed $url
	 */
	public function setUrl($url)
	{
		$this->url = $url;
	}

	/**
	 * @return mixed
	 */
	public function getOriginalName()
	{
		return $this->originalName;
	}

	/**
	 * @param mixed $originalName
	 */
	public function setOriginalName($originalName)
	{
		$this->originalName = $originalName;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		return [
			'url' => $this->getUrl(),
			'originalName' => $this->getOriginalName()
		];
	}
}