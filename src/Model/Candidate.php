<?php


namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class Candidate
 *
 * @package Hercul\Hercul\Model
 */
class Candidate implements JsonSerializable
{

	/** @var */
	private $firstName;

	/** @var */
	private $lastName;

	/** @var */
	private $email;

	/** @var0 */
	private $phone;

	/** @var */
	private $avatar;

	/** @var Location */
	private $location;

	/** @var */
	private $gender;

	/** @var  */
	private $age;

	/** @var  */
	private $qualificationId;

	/** @var  */
	private $birthDate;

	/**
	 * @return mixed
	 */
	public function getFirstName()
	{
		return $this->firstName;
	}

	/**
	 * @param mixed $firstName
	 */
	public function setFirstName($firstName)
	{
		$this->firstName = $firstName;
	}

	/**
	 * @return mixed
	 */
	public function getLastName()
	{
		return $this->lastName;
	}

	/**
	 * @param mixed $lastName
	 */
	public function setLastName($lastName)
	{
		$this->lastName = $lastName;
	}

	/**
	 * @return mixed
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @param mixed $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}

	/**
	 * @return mixed
	 */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * @param mixed $phone
	 */
	public function setPhone($phone)
	{
		$this->phone = $phone;
	}

	/**
	 * @return mixed
	 */
	public function getAvatar()
	{
		return $this->avatar;
	}

	/**
	 * @param mixed $avatar
	 */
	public function setAvatar($avatar)
	{
		$this->avatar = $avatar;
	}

	/**
	 * @return Location
	 */
	public function getLocation(): Location
	{
		return $this->location;
	}

	/**
	 * @param Location $location
	 */
	public function setLocation(Location $location)
	{
		$this->location = $location;
	}

	/**
	 * @return mixed
	 */
	public function getGender()
	{
		return $this->gender;
	}

	/**
	 * @param mixed $gender
	 */
	public function setGender($gender)
	{
		$this->gender = $gender;
	}

	/**
	 * @return mixed
	 */
	public function getAge()
	{
		return $this->age;
	}

	/**
	 * @param mixed $age
	 */
	public function setAge($age)
	{
		$this->age = $age;
	}

	/**
	 * @return mixed
	 */
	public function getQualificationId()
	{
		return $this->qualificationId;
	}

	/**
	 * @param mixed $qualificationId
	 */
	public function setQualificationId($qualificationId)
	{
		$this->qualificationId = $qualificationId;
	}

	/**
	 * @return mixed
	 */
	public function getBirthDate()
	{
		return $this->birthDate;
	}

	/**
	 * @param mixed $birthDate
	 */
	public function setBirthDate($birthDate)
	{
		$this->birthDate = $birthDate;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		$candidate = [
			'firstName' => $this->getFirstName(),
			'lastName' => $this->getLastName(),
			'email' => $this->getEmail(),
			'phone' => $this->getPhone(),
			'location' => $this->getLocation(),
			'gender' => $this->getGender(),
			'age' => $this->getAge(),
			'qualificationId' => $this->getQualificationId()
		];

		$birthDate = $this->getBirthDate();
		if (!empty($birthDate)) {
			$candidate['birthDate'] = $birthDate;
		}

		$avatar = $this->getAvatar();
		if (!empty($avatar)) {
			$candidate['avatar'] = $avatar;
		}

		return $candidate;
	}
}