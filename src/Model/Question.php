<?php

namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class Questionnaire
 *
 * @package Hercul\Hercul\Model
 */
class Question implements JsonSerializable
{

	/** @var  */
	private $externalId;

	/** @var  */
	private $type;

	/** @var  */
	private $title;

	/** @var  */
	private $description;

	/** @var bool  */
	private $allowsMultipleChoices = false;

	/** @var bool  */
	private $isRequired = false;

	/** @var  */
	private $choices;

	/** @var  */
	private $position;

	/**
	 * @return mixed
	 */
	public function getExternalId()
	{
		return $this->externalId;
	}

	/**
	 * @param mixed $externalId
	 */
	public function setExternalId($externalId)
	{
		$this->externalId = $externalId;
	}

	/**
	 * @return mixed
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param mixed $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}

	/**
	 * @return mixed
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param mixed $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * @return mixed
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param mixed $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * @return bool
	 */
	public function isAllowsMultipleChoices(): bool
	{
		return $this->allowsMultipleChoices;
	}

	/**
	 * @param bool $allowsMultipleChoices
	 */
	public function setAllowsMultipleChoices(bool $allowsMultipleChoices)
	{
		$this->allowsMultipleChoices = $allowsMultipleChoices;
	}
	/**
	 * @return bool
	 */
	public function isRequired(): bool
	{
		return $this->isRequired;
	}

	/**
	 * @param bool $isRequired
	 */
	public function setIsRequired(bool $isRequired)
	{
		$this->isRequired = $isRequired;
	}

	/**
	 * @return mixed
	 */
	public function getChoices()
	{
		return $this->choices;
	}

	/**
	 * @param mixed $choices
	 */
	public function setChoices($choices)
	{
		$this->choices = $choices;
	}

	/**
	 * @return mixed
	 */
	public function getPosition()
	{
		return $this->position;
	}

	/**
	 * @param mixed $position
	 */
	public function setPosition($position)
	{
		$this->position = $position;
	}

	/**
	 * @return array
	 */
	public function jsonSerialize()
	{
		$question = [
			'type' => $this->getType(),
			'title' => $this->getTitle(),
			'externalId' => $this->getExternalId(),
			'required' => $this->isRequired(),
			'position' => $this->getPosition()
		];

		$choices = $this->getChoices();
		if (!empty($choices)) {
			$question['choices'] = $choices;
		}

		return $question;
	}
}