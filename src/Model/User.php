<?php


namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class User
 *
 * @package Hercul\Hercul\Model
 */
class User implements JsonSerializable
{

	/** @var  */
	private $id;

	/** @var  */
	private $externalId;

	/** @var  */
	private $firstName;

	/** @var  */
	private $lastName;

	/** @var  */
	private $email;

	/** @var  */
	private $newEmail = null;

	/** @var  */
	private $phone;

	/** @var */
	private $avatar;

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id): void
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getExternalId()
	{
		return $this->externalId;
	}

	/**
	 * @param mixed $externalId
	 */
	public function setExternalId($externalId)
	{
		$this->externalId = $externalId;
	}

	/**
	 * @return mixed
	 */
	public function getFirstName()
	{
		return $this->firstName;
	}

	/**
	 * @param mixed $firstName
	 */
	public function setFirstName($firstName)
	{
		$this->firstName = $firstName;
	}

	/**
	 * @return mixed
	 */
	public function getLastName()
	{
		return $this->lastName;
	}

	/**
	 * @param mixed $lastName
	 */
	public function setLastName($lastName)
	{
		$this->lastName = $lastName;
	}

	/**
	 * @return mixed
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @param mixed $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}

	/**
	 * @return mixed
	 */
	public function getNewEmail()
	{
		return $this->newEmail;
	}

	/**
	 * @param mixed $newEmail
	 */
	public function setNewEmail($newEmail): void
	{
		$this->newEmail = $newEmail;
	}

	/**
	 * @return mixed
	 */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * @param mixed $phone
	 */
	public function setPhone($phone)
	{
		$this->phone = $phone;
	}

	/**
	 * @return mixed
	 */
	public function getAvatar()
	{
		return $this->avatar;
	}

	/**
	 * @param mixed $avatar
	 */
	public function setAvatar($avatar)
	{
		$this->avatar = $avatar;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		$user = [
			'id' => $this->getId(),
			'externalId' => $this->getExternalId(),
			'firstName' => $this->getFirstName(),
			'lastName' => $this->getLastName(),
			'email' => $this->getEmail(),
			'phone' => $this->getPhone()
		];

		$avatar = $this->getAvatar();
		if (!empty($avatar)) {
			$user['avatar'] = $avatar;
		}

		$newEmail = $this->getNewEmail();
		if (!is_null($newEmail)) {
			$user['newEmail'] = $newEmail;
		}

		return $user;
	}
}