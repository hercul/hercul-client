<?php

namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class ApplicationQuestion
 *
 * @package Hercul\Hercul\Model
 */
class ApplicationQuestion implements JsonSerializable
{

	/** @var  */
	private $id;

	/** @var  */
	private $externalId;

	/** @var  */
	private $answers;

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id): void
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getExternalId()
	{
		return $this->externalId;
	}

	/**
	 * @param mixed $externalId
	 */
	public function setExternalId($externalId)
	{
		$this->externalId = $externalId;
	}

	/**
	 * @return mixed
	 */
	public function getAnswers()
	{
		return $this->answers;
	}

	/**
	 * @param $answers
	 */
	public function setAnswers($answers)
	{
		$this->answers = $answers;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		$applicationQuestions = [
			'id' => $this->getId(),
			'externalId' => $this->getExternalId()
		];

		$answers = $this->getAnswers();
		if (!empty($answers)) {
			$applicationQuestions['answers'] = $answers;
		}

		return $applicationQuestions;
	}
}