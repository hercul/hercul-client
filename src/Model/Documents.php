<?php

namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class Documents
 *
 * @package Hercul\Hercul\Model
 */
class Documents implements JsonSerializable
{

	/** @var []  */
	private $documents = [];

	/**
	 * @return array
	 */
	public function getDocuments(): array
	{
		return $this->documents;
	}

	/**
	 * @param Document $document
	 */
	public function addDocument(Document $document)
	{
		$this->documents[] = $document;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		return $this->getDocuments();
	}
}