<?php


namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class CompanyRole
 *
 * @package Hercul\Hercul\Model
 */
class CompanyRole implements JsonSerializable
{

	/** @var  */
	private $admin = null;

	/** @var  */
	private $user;

	/** @var  */
	private $companyId;

	/** @var  */
	private $roleName;

	/**
	 * @return mixed
	 */
	public function getAdmin()
	{
		return $this->admin;
	}

	/**
	 * @param mixed $admin
	 */
	public function setAdmin($admin)
	{
		$this->admin = $admin;
	}

	/**
	 * @return mixed
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * @param mixed $user
	 */
	public function setUser($user)
	{
		$this->user = $user;
	}

	/**
	 * @return mixed
	 */
	public function getCompanyId()
	{
		return $this->companyId;
	}

	/**
	 * @param mixed $companyId
	 */
	public function setCompanyId($companyId)
	{
		$this->companyId = $companyId;
	}

	/**
	 * @return mixed
	 */
	public function getRoleName()
	{
		return $this->roleName;
	}

	/**
	 * @param mixed $roleName
	 */
	public function setRoleName($roleName)
	{
		$this->roleName = $roleName;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		$companyRole = [
			'user' => $this->getUser(),
			'companyId' => $this->getCompanyId(),
			'roleName' => $this->getRoleName()
		];

		$admin = $this->getAdmin();
		if (!empty($admin)) {
			$companyRole['admin'] = $admin;
		}

		return $companyRole;
	}
}