<?php

namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class Questionnaire
 *
 * @package Hercul\Hercul\Model
 */
class Answer implements JsonSerializable
{

	/** @var  */
	private $id;

	/** @var */
	private $externalId;

	/** @var  */
	private $name;

	/** @var  */
	private $value;

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId($id): void
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return mixed
	 */
	public function getExternalId()
	{
		return $this->externalId;
	}

	/**
	 * @param mixed $externalId
	 */
	public function setExternalId($externalId)
	{
		$this->externalId = $externalId;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @param mixed $value
	 */
	public function setValue($value)
	{
		$this->value = $value;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		$answer = [
			'id' => $this->getId()
		];


		$externalId = $this->getExternalId();
		if (!empty($externalId)) {
			$answer['externalId'] = $externalId;
		}

		$name =$this->getName();
		if (!empty($name)) {
			$answer[$this->getName()] = $this->getValue();
		}

		return $answer;
	}
}