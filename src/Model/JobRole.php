<?php


namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class JobRole
 *
 * @package Hercul\Hercul\Model
 */
class JobRole implements JsonSerializable
{

	/** @var  */
	private $admin = null;

	/** @var  */
	private $user;

	/** @var  */
	private $jobId;

	/** @var  */
	private $jobUuid;

	/** @var  */
	private $roleName;

	/**
	 * @return mixed
	 */
	public function getAdmin()
	{
		return $this->admin;
	}

	/**
	 * @param mixed $admin
	 */
	public function setAdmin($admin)
	{
		$this->admin = $admin;
	}

	/**
	 * @return mixed
	 */
	public function getUser()
	{
		return $this->user;
	}

	/**
	 * @param mixed $user
	 */
	public function setUser($user)
	{
		$this->user = $user;
	}

	/**
	 * @return mixed
	 */
	public function getJobId()
	{
		return $this->jobId;
	}

	/**
	 * @param mixed $jobId
	 */
	public function setJobId($jobId)
	{
		$this->jobId = $jobId;
	}

	/**
	 * @return mixed
	 */
	public function getJobUuid()
	{
		return $this->jobUuid;
	}

	/**
	 * @param mixed $jobUuid
	 */
	public function setJobUuid($jobUuid): void
	{
		$this->jobUuid = $jobUuid;
	}

	/**
	 * @return mixed
	 */
	public function getRoleName()
	{
		return $this->roleName;
	}

	/**
	 * @param mixed $roleName
	 */
	public function setRoleName($roleName)
	{
		$this->roleName = $roleName;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		$jobRole = [
			'user' => $this->getUser(),
			'jobId' => $this->getJobId(),
			'jobUuid' => $this->getJobUuid(),
			'roleName' => $this->getRoleName()
		];

		$admin = $this->getAdmin();
		if (!empty($admin)) {
			$jobRole['admin'] = $admin;
		}

		return $jobRole;
	}
}