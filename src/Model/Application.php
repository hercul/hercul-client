<?php


namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class Application
 *
 * @package Hercul\Hercul\Model
 */
class Application implements JsonSerializable
{

	/** @var int */
	private $id;

	/** @var */
	private $uuid = null;

	/** @var int */
	private $jobId;

	/** @var  */
	private $jobUuid;

	/** @var */
	private $candidate;

	/** @var */
	private $letter;

	/** @var  */
	private $applicationTime;

	/** @var */
	private $questions;

	/** @var */
	private $documents;

	/** @var  */
	private $views;

	/** @var null  */
	private $requisitionId = null;

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getUuid()
	{
		return $this->uuid;
	}

	/**
	 * @param mixed $uuid
	 */
	public function setUuid($uuid): void
	{
		$this->uuid = $uuid;
	}

	/**
	 * @return int
	 */
	public function getJobId(): int
	{
		return $this->jobId;
	}

	/**
	 * @param int $jobId
	 */
	public function setJobId(int $jobId)
	{
		$this->jobId = $jobId;
	}

	/**
	 * @return mixed
	 */
	public function getJobUuid()
	{
		return $this->jobUuid;
	}

	/**
	 * @param mixed $jobUuid
	 */
	public function setJobUuid($jobUuid): void
	{
		$this->jobUuid = $jobUuid;
	}

	/**
	 * @return mixed
	 */
	public function getCandidate()
	{
		return $this->candidate;
	}

	/**
	 * @param mixed $candidate
	 */
	public function setCandidate($candidate)
	{
		$this->candidate = $candidate;
	}

	/**
	 * @return mixed
	 */
	public function getLetter()
	{
		return $this->letter;
	}

	/**
	 * @param mixed $letter
	 */
	public function setLetter($letter)
	{
		$this->letter = $letter;
	}

	/**
	 * @return mixed
	 */
	public function getApplicationTime()
	{
		return $this->applicationTime;
	}

	/**
	 * @param mixed $applicationTime
	 */
	public function setApplicationTime($applicationTime)
	{
		$this->applicationTime = $applicationTime;
	}

	/**
	 * @return mixed
	 */
	public function getQuestions()
	{
		return $this->questions;
	}

	/**
	 * @param mixed $questions
	 */
	public function setQuestions($questions)
	{
		$this->questions = $questions;
	}

	/**
	 * @return mixed
	 */
	public function getDocuments()
	{
		return $this->documents;
	}

	/**
	 * @param mixed $documents
	 */
	public function setDocuments($documents)
	{
		$this->documents = $documents;
	}

	/**
	 * @return mixed
	 */
	public function getViews()
	{
		return $this->views;
	}

	/**
	 * @param mixed $views
	 */
	public function setViews($views)
	{
		$this->views = $views;
	}

	/**
	 * @return null
	 */
	public function getRequisitionId()
	{
		return $this->requisitionId;
	}

	/**
	 * @param null $requisitionId
	 */
	public function setRequisitionId($requisitionId): void
	{
		$this->requisitionId = $requisitionId;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		$application = [
			'id' => $this->getId(),
			'uuid' => $this->getUuid(),
			'candidate' => $this->getCandidate(),
			'appliedAt' => $this->getApplicationTime(),
			'totalViews' => $this->getViews(),
			'jobId' => $this->getJobId(),
			'jobUuid' => $this->getJobUuid(),
			'requisitionId' => $this->getRequisitionId()
		];

		$questions = $this->getQuestions();
		if (!empty($questions)) {
			$application['questions'] = $questions;
		}

		$documents = $this->getDocuments();
		if (!empty($documents)) {
			$application['documents'] = $documents;
		}

		$letter = $this->getLetter();
		if (!empty($letter)) {
			$application['letter'] = $letter;
		} else {
			$application['letter'] = '';
		}

		return $application;
	}
}