<?php

namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class Choices
 *
 * @package Hercul\Hercul\Model
 */
class Choices implements JsonSerializable
{
	private $choices = [];

	/**
	 * @return array
	 */
	public function getChoices(): array
	{
		return $this->choices;
	}

	/**
	 * @param Choice $choice
	 */
	public function addChoice(Choice $choice)
	{
		$this->choices[] = $choice;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		return $this->getChoices();
	}

}