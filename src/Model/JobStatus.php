<?php

namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class JobStatus
 *
 * @package Hercul\Hercul\Model
 */
class JobStatus implements JsonSerializable
{

	/** @var  */
	private $adId;

	/** @var  */
	private $jobUuid;

	/** @var */
	private $status;

	/** @var */
	private $publishDate;

	/** @var */
	private $expirationDate;

	/**
	 * @return mixed
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param mixed $status
	 */
	public function setStatus($status): void
	{
		$this->status = $status;
	}

	/**
	 * @return mixed
	 */
	public function getPublishDate()
	{
		return $this->publishDate;
	}

	/**
	 * @param mixed $publishDate
	 */
	public function setPublishDate($publishDate): void
	{
		$this->publishDate = $publishDate;
	}

	/**
	 * @return mixed
	 */
	public function getExpirationDate()
	{
		return $this->expirationDate;
	}

	/**
	 * @param mixed $expirationDate
	 */
	public function setExpirationDate($expirationDate): void
	{
		$this->expirationDate = $expirationDate;
	}

	/**
	 * @param mixed $adId
	 */
	public function setAdId($adId): void
	{
		$this->adId = $adId;
	}

	/**
	 * @return mixed
	 */
	public function getAdId()
	{
		return $this->adId;
	}

	/**
	 * @return mixed
	 */
	public function getJobUuid()
	{
		return $this->jobUuid;
	}

	/**
	 * @param mixed $jobUuid
	 */
	public function setJobUuid($jobUuid): void
	{
		$this->jobUuid = $jobUuid;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		return [
			'jobId' => $this->getAdId(),
			'jobUuid' => $this->getJobUuid(),
			'status' => $this->getStatus(),
			'publishDate' => $this->getPublishDate(),
			'expiredDate' => $this->getExpirationDate(),
		];
	}
}