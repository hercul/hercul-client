<?php

namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class Questionnaire
 *
 * @package Hercul\Hercul\Model
 */
class Answers implements JsonSerializable
{

	/** @var array  */
	private $answers = [];

	/**
	 * @return array
	 */
	public function getAnswers(): array
	{
		return $this->answers;
	}

	/**
	 * @param $answers
	 */
	public function addAnswer($answers)
	{
		$this->answers[] = $answers;
	}

	/**
	 * @return array
	 */
	public function jsonSerialize()
	{
		return $this->getAnswers();
	}

}