<?php

namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class Job
 *
 * @package Hercul\Hercul\Model
 */
class Job implements JsonSerializable
{
	/** @var int */
	private $id;

	/** @var */
	private $uuid = null;

	/** @var */
	private $title;

	/** @var */
	private $description;

	/** @var */
	private $status;

	/** @var */
	private $publishDate;

	/** @var */
	private $expirationDate;

	/** @var */
	private $location;

	/** @var int */
	private $companyId;

	/** @var */
	private $jobLink;

	/** @var bool  */
	private $removeQuestionnaire = false;

	/** @var  */
	private $questionnaireId= null;

	/** @var bool */
	private $applicationLink;

	/** @var */
	private $notificationType;

	/** @var null  */
	private $requisitionId = null;

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getUuid()
	{
		return $this->uuid;
	}

	/**
	 * @param mixed $uuid
	 */
	public function setUuid($uuid): void
	{
		$this->uuid = $uuid;
	}

	/**
	 * @return mixed
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param mixed $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * @return mixed
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param mixed $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * @return mixed
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param mixed $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/**
	 * @return mixed
	 */
	public function getPublishDate()
	{
		return $this->publishDate;
	}

	/**
	 * @param mixed $publishDate
	 */
	public function setPublishDate($publishDate)
	{
		$this->publishDate = $publishDate;
	}

	/**
	 * @return mixed
	 */
	public function getExpirationDate()
	{
		return $this->expirationDate;
	}

	/**
	 * @param mixed $expirationDate
	 */
	public function setExpirationDate($expirationDate)
	{
		$this->expirationDate = $expirationDate;
	}

	/**
	 * @return mixed
	 */
	public function getLocation()
	{
		return $this->location;
	}

	/**
	 * @param mixed $location
	 */
	public function setLocation($location)
	{
		$this->location = $location;
	}

	/**
	 * @return int
	 */
	public function getCompanyId(): int
	{
		return $this->companyId;
	}

	/**
	 * @param int $companyId
	 */
	public function setCompanyId(int $companyId)
	{
		$this->companyId = $companyId;
	}

	/**
	 * @return mixed
	 */
	public function getJobLink()
	{
		return $this->jobLink;
	}

	/**
	 * @param mixed $jobLink
	 */
	public function setJobLink($jobLink)
	{
		$this->jobLink = $jobLink;
	}

	/**
	 * @return bool
	 */
	public function getRemoveQuestionnaire()
	{
		return $this->removeQuestionnaire;
	}

	/**
	 * @param bool $removeQuestionnaire
	 */
	public function setRemoveQuestionnaire($removeQuestionnaire)
	{
		$this->removeQuestionnaire = $removeQuestionnaire;
	}

	/**
	 * @return mixed
	 */
	public function getQuestionnaireId()
	{
		return $this->questionnaireId;
	}

	/**
	 * @param mixed $questionnaireId
	 */
	public function setQuestionnaireId($questionnaireId)
	{
		$this->questionnaireId = $questionnaireId;
	}

	/**
	 * @return bool
	 */
	public function getApplicationLink(): bool
	{
		return $this->applicationLink;
	}

	/**
	 * @param bool $applicationLink
	 */
	public function setApplicationLink(bool $applicationLink): void
	{
		$this->applicationLink = $applicationLink;
	}

	/**
	 * @return mixed
	 */
	public function getNotificationType()
	{
		return $this->notificationType;
	}

	/**
	 * @param mixed $notificationType
	 */
	public function setNotificationType($notificationType): void
	{
		$this->notificationType = $notificationType;
	}

	/**
	 * @return null
	 */
	public function getRequisitionId()
	{
		return $this->requisitionId;
	}

	/**
	 * @param null $requisitionId
	 */
	public function setRequisitionId($requisitionId): void
	{
		$this->requisitionId = $requisitionId;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		return [
			'title' => $this->getTitle(),
			'description' => $this->getDescription(),
			'externalStatus' => $this->getStatus(),
			'publishDate' => $this->getPublishDate(),
			'expirationDate' => $this->getExpirationDate(),
			'location' => $this->getLocation(),
			'companyId' => $this->getCompanyId(),
			'id' => $this->getId(),
			'uuid' => $this->getUuid(),
			'jobLink' => $this->getJobLink(),
			'removeQuestionnaire' => $this->getRemoveQuestionnaire(),
			'questionnaireId' => $this->getQuestionnaireId(),
			'externalApplicationLink' => $this->getApplicationLink(),
			'notificationType' => $this->getNotificationType(),
			'requisitionId' => $this->getRequisitionId(),
		];
	}
}