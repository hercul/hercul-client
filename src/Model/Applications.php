<?php

namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class Applications
 *
 * @package Hercul\Hercul\Model
 */
class Applications implements JsonSerializable
{

	/** @var []  */
	private $applications = [];

	/**
	 * @return array
	 */
	public function getApplications()
	{
		return $this->applications;
	}

	/**
	 * @param Application $application
	 */
	public function addApplication(Application $application)
	{
		$this->applications[] = $application;
	}

	/**
	 * @return array
	 */
	public function jsonSerialize()
	{
		return $this->getApplications();
	}
}