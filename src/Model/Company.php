<?php

namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class Company
 *
 * @package Hercul\Hercul\Model
 */
class Company implements JsonSerializable
{

	/** @var int */
    private $id;

    /** @var */
    private $name;

	/** @var */
    private $description;

	/** @var */
    private $phone;

    /** @var */
    private $taxNumber;

    /** @var */
    private $location;

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param mixed $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * @return mixed
	 */
	public function getPhone()
	{
		return $this->phone;
	}

	/**
	 * @param mixed $phone
	 */
	public function setPhone($phone)
	{
		$this->phone = $phone;
	}

	/**
	 * @return mixed
	 */
	public function getTaxNumber()
	{
		return $this->taxNumber;
	}

	/**
	 * @param mixed $taxNumber
	 */
	public function setTaxNumber($taxNumber)
	{
		$this->taxNumber = $taxNumber;
	}

	/**
	 * @return mixed
	 */
	public function getLocation()
	{
		return $this->location;
	}

	/**
	 * @param mixed $location
	 */
	public function setLocation($location)
	{
		$this->location = $location;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		return [
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'phone' => $this->getPhone(),
			'taxNumber' => $this->getTaxNumber(),
			'location' => $this->getLocation(),
		];
	}
}