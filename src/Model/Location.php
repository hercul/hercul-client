<?php

namespace Hercul\Hercul\Model;

use JsonSerializable;

/**
 * Class Location
 *
 * @package Hercul\Hercul\Model
 */
class Location implements JsonSerializable
{

	/** @var string */
	private $address;

	/** @var string */
	private $place;

	/** @var string */
	private $city;

	/** @var string */
	private $country;

	/** @var int */
	private $zip;

	/**
	 * @return string
	 */
	public function getAddress(): string
	{
		return $this->address;
	}

	/**
	 * @param string $address
	 */
	public function setAddress(string $address)
	{
		$this->address = $address;
	}

	/**
	 * @return string
	 */
	public function getPlace(): string
	{
		return $this->place;
	}

	/**
	 * @param string $place
	 */
	public function setPlace(string $place)
	{
		$this->place = $place;
	}

	/**
	 * @return string
	 */
	public function getCity(): string
	{
		return $this->city;
	}

	/**
	 * @param string $city
	 */
	public function setCity(string $city)
	{
		$this->city = $city;
	}

	/**
	 * @return string
	 */
	public function getCountry(): string
	{
		return $this->country;
	}

	/**
	 * @param string $country
	 */
	public function setCountry(string $country)
	{
		$this->country = $country;
	}

	/**
	 * @return int
	 */
	public function getZip(): int
	{
		return $this->zip;
	}

	/**
	 * @param int $zip
	 */
	public function setZip(int $zip)
	{
		$this->zip = $zip;
	}

	/**
	 * @return array|mixed
	 */
	public function jsonSerialize()
	{
		return [
			'address' => $this->getAddress(),
			'place' => $this->getPlace(),
			'city' => $this->getCity(),
			'country' => $this->getCountry(),
			'zip' => $this->getZip()
		];
	}
}